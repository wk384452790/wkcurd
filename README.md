# wkcurd

### 介绍

wkcurd可以用命令直接生成 模型，验证，控制器的一个小便携工具

### 安装教程

 composer require wk384452790/wkcurd dev-master
 

### 使用说明

####php think wkcurd -t mall_goods -m mobile@mall_goods -a mobile@mall_goods  

1.  -t      表名(必填,不带前缀)
    
2.  -c      控制器名称(选填,不指定目录默认会创建common/controller文件夹,如果想在api目录下:api@goods即可)
    
3.  -m      模型名称(选填,不指定目录默认会创建common/model文件夹,如果想在api目录下:api@goods即可)
    
4.  -a      验证类名称(选填,不指定目录默认会创建common/validate文件夹,如果想在api目录下:api@goodsValidate即可)
    
5.  -r      创建时间字段(选填,默认是create_time)
    
6.  -k      数据库字段主键(选填,默认是id)
    
7.  -p      更新时间字段(选填,默认是update_time)
    
8.  -d      是否删除字段(选填,默认是is_delete)

###注意

1.controller,model,validate 必须选择一个。

2.如果只生成控制器的话需要选择指定对应的model和validate的路径。

3.已经生成的文件不会覆盖，如果需要重新生成请先删除。


